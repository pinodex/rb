# RB

Node.js Typescript + MongoDB + Mongoose Project

## Prerequisite

* Node.js >= 16.14
* Docker
* Docker Compose

## Setup

1. Install dependencies

    ```shell
    npm install
    ```

2. Start backend services

    ```shell
    docker-compose up -d
    ```

## Usage

Run an input file through the script.

```shell
npm run start:dev -- -i ./test-data/test-data-1.txt
```

## Lint

```shell
npm run lint
```
