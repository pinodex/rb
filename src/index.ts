import 'dotenv/config';

import { ArgumentParser } from 'argparse';
import { connect } from 'mongoose';

import { GradeParser } from './grade-parser';
import { GradeSaver } from './grade-saver';

async function main(inputFileName: string) {
  const db = await connect(process.env.MONGODB_URI);

  const gradeParser = new GradeParser(inputFileName);
  const gradeSaver = new GradeSaver();

  await gradeParser.readFile();

  await gradeSaver.upsertStudentAverages(gradeParser.getAveragesPerStudent());

  const averagesPerStudent = await gradeSaver.getOrderedStudentAverages();

  process.stdout.write('Averages\n');

  averagesPerStudent.forEach(({ name, average }) => {
    process.stdout.write(`${name} ${average}\n`);
  });

  db.disconnect();
}

const parser = new ArgumentParser();
parser.add_argument('-i', '--input', { required: true });

const { input } = parser.parse_args();

main(input);
