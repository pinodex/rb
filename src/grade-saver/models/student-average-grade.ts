import { model, Schema } from 'mongoose';

export interface StudentAverageGrade {
  name: string;
  average: number;
}

export const studentAverageGradeSchema = new Schema<StudentAverageGrade>({
  name: {
    type: String,
    required: true,
    index: true,
  },

  average: {
    type: Number,
    required: true,
  },
});

export const StudentAverageGradeModel = model<StudentAverageGrade>(
  'StudentAverageGrade',
  studentAverageGradeSchema,
);

export type StudentAverageGradeType = typeof StudentAverageGradeModel;
