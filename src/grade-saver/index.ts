import { StudentAverage } from '@/grade-parser/interfaces';

import { StudentAverageGradeModel } from './models/student-average-grade';

export class GradeSaver {
  /**
   * Update or insert student averages
   */
  async upsertStudentAverages(
    studentAverages: StudentAverage[],
  ): Promise<void> {
    await Promise.all(
      studentAverages.map(async ({ name, average }) => {
        await StudentAverageGradeModel.findOneAndUpdate(
          { name },
          { average },
          { upsert: true },
        );
      }),
    );
  }

  /**
   * Get student averages ordered by average
   */
  async getOrderedStudentAverages(): Promise<StudentAverage[]> {
    const studentAverageGrades = await StudentAverageGradeModel.find().sort({
      name: 1,
    });

    return studentAverageGrades.map(({ name, average }) => ({ name, average }));
  }
}
