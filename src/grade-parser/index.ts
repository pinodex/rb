import fs, { PathLike, ReadStream } from 'fs';
import { createInterface, Interface } from 'readline';

import {
  getAverage,
  getLineTokens,
  normalizeString,
  removeLowestNumber,
} from './helpers';
import {
  LineType,
  Quarter,
  QuarterStudentAverage,
  Student,
  StudentAverage,
  StudentLineTokenType,
} from './interfaces';

export class GradeParser {
  protected file: ReadStream;

  protected reader: Interface;

  protected quarters: Quarter[] = [];

  constructor(protected readonly path: PathLike) {
    this.file = fs.createReadStream(path, {
      encoding: 'utf8',
    });

    this.reader = createInterface({
      input: this.file,
      crlfDelay: Infinity,
    });
  }

  /**
   * Determines the type
   * of the line whether it is a line that defines a quarter,
   * or a line that defines a grade.
   *
   * @param raw Line from grade text input
   */
  protected determineLineType(raw: string): LineType {
    if (raw.trim().length === 0) {
      return LineType.INVALID;
    }

    if (raw.startsWith('Quarter')) {
      return LineType.QUARTER;
    }

    return LineType.STUDENT;
  }

  /**
   * Parses line string for student into Student object. Determines
   * the homework grades and test grades for the student.
   *
   * @param raw Line string for student
   */
  protected parseStudentLine(raw: string): Student {
    const tokens = getLineTokens(raw);

    const nameTokens: string[] = [];
    const homeworkTokens: string[] = [];
    const testTokens: string[] = [];

    let currentTokenType = StudentLineTokenType.NAME;

    tokens.forEach((token) => {
      if (
        Object.values(StudentLineTokenType).includes(
          token as StudentLineTokenType,
        ) &&
        token !== currentTokenType
      ) {
        currentTokenType = token as StudentLineTokenType;
        return;
      }

      switch (currentTokenType) {
        case StudentLineTokenType.NAME:
          nameTokens.push(token);
          break;

        case StudentLineTokenType.HOMEWORK:
          homeworkTokens.push(token);
          break;

        case StudentLineTokenType.TEST:
          testTokens.push(token);
          break;
      }
    });

    const name = nameTokens.join(' ');

    const homeworkGrades = homeworkTokens
      .map((token) => parseFloat(token))
      .filter((grade) => !isNaN(grade));

    const testGrades = testTokens
      .map((token) => parseFloat(token))
      .filter((grade) => !isNaN(grade));

    return { name, homeworkGrades, testGrades };
  }

  /**
   * Parses line string for quarter into Quarter object.
   *
   * @param raw Line string for quarter
   */
  protected parseQuarterLine(raw: string): Quarter {
    const [, countString, yearString] = raw.match(/Quarter (\d+), (\d+)/);

    return {
      count: parseInt(countString),
      year: parseInt(yearString),
      students: [],
    };
  }

  async readFile(): Promise<void> {
    return new Promise((resolve) => {
      let currentQuarter: Quarter;
      let previousLineType: LineType;

      this.reader.on('line', (rawLine) => {
        const line = normalizeString(rawLine);
        const lineType = this.determineLineType(line);

        if (lineType === LineType.INVALID) {
          return;
        }

        // Check if the previous line is a quarter line
        // and the current line is a student line before
        // adding the current quarter to the array of quarters.
        if (
          previousLineType &&
          currentQuarter &&
          previousLineType === LineType.QUARTER &&
          lineType === LineType.STUDENT
        ) {
          this.quarters.push(currentQuarter);
        }

        previousLineType = lineType;

        if (lineType === LineType.QUARTER) {
          currentQuarter = this.parseQuarterLine(line);
        }

        if (lineType === LineType.STUDENT && currentQuarter) {
          const student = this.parseStudentLine(line);
          currentQuarter.students.push(student);
        }
      });

      this.reader.on('close', () => resolve());
    });
  }

  /**
   * Get list of quarters with students
   */
  getQuarters(): Quarter[] {
    return this.quarters;
  }

  /**
   * Compute the average grade per student per quarter
   */
  getAveragesPerStudentQuarter(): QuarterStudentAverage[] {
    return this.quarters.map((quarter) => {
      const students = quarter.students.map<StudentAverage>(
        ({ name, homeworkGrades, testGrades }) => {
          const homeworkAverage = getAverage(
            removeLowestNumber(homeworkGrades),
          );

          const testAverage = getAverage(testGrades);

          const average =
            Math.round((homeworkAverage * 0.4 + testAverage * 0.6) * 10) / 10;

          return { name, average };
        },
      );

      return {
        count: quarter.count,
        year: quarter.year,
        students,
      };
    });
  }

  /**
   * Compute the average grade per student for all quarters
   */
  getAveragesPerStudent(): StudentAverage[] {
    const computedQuarters = this.getAveragesPerStudentQuarter();
    const studentAverages: Record<string, number[]> = {};

    // Get all students across all quarters
    const allStudents = computedQuarters.reduce<StudentAverage[]>(
      (accumulatedStudents, currentQuarter) =>
        accumulatedStudents.concat(currentQuarter.students),
      [],
    );

    allStudents.forEach(({ name, average }) => {
      if (!Object.keys(studentAverages).includes(name)) {
        studentAverages[name] = [];
      }

      studentAverages[name].push(average);
    });

    return Object.keys(studentAverages).map((name) => ({
      name,
      average: getAverage(studentAverages[name]),
    }));
  }
}
