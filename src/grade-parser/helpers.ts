/**
 * Normalize string to be safe for parsing. This strips the control
 * characters, normalizes newlines, and normalizes spaces.
 */
export function normalizeString(text: string): string {
  return (
    text
      // strip control chars
      .replace(/\p{C}/gu, '')

      // normalize newline
      .replace(/\n\r/g, '\n')
      .replace(/\p{Zl}/gu, '\n')
      .replace(/\p{Zp}/gu, '\n')

      // normalize space
      .replace(/\p{Zs}/gu, ' ')
  );
}

/**
 * Split string by space and normalize each element of the split
 * to represent a token for parsing
 */
export function getLineTokens(raw: string): string[] {
  return raw.split(' ').map((token) => normalizeString(token.trim()));
}

/**
 * Compute average of an array of numbers
 */
export function getAverage(values: number[]): number {
  const sum = values.reduce(
    (previousTotal, currentTotal) => previousTotal + currentTotal,
    0,
  );

  return sum / values.length;
}

/**
 * Remove the lowest number from the array of numbers
 */
export function removeLowestNumber(values: number[]): number[] {
  const sortedValues = [...values].sort((a, b) => a - b);

  // Remove first element
  sortedValues.shift();

  return sortedValues;
}
