export interface Student {
  name: string;
  homeworkGrades: number[];
  testGrades: number[];
}

export interface Quarter {
  count: number;
  year: number;
  students: Student[];
}

export interface StudentAverage {
  name: string;
  average: number;
}

export interface QuarterStudentAverage extends Omit<Quarter, 'students'> {
  students: StudentAverage[];
}

export enum StudentLineTokenType {
  NAME = 'NAME',
  HOMEWORK = 'H',
  TEST = 'T',
}

export enum LineType {
  QUARTER = 'QUARTER',
  STUDENT = 'STUDENT',
  INVALID = 'INVALID',
}

export interface Line {
  type: LineType;
  raw: string;
}
